'use strict';

trackMe.controller('ProgressBarController',
    function ProgressBarController($scope){
        $scope.sort = function(keyname){
            $scope.sortKey = keyname;
            $scope.reverse = !$scope.reverse;
        };
        $scope.today = function(){
            var dt = new Date();
            return dt.toDateString();
        };
        $scope.getTime = function(){
            var dt = new Date();
            return dt.toTimeString();
        };
        $scope.getSeconds = function(str){
            var hms = str;
            var hm = hms.split(':');
            var s = hm[2].split(' ');
            var seconds = (+hm[0]) * 60 * 60 + (+hm[1]) * 60 + (+s[0]);
            return(seconds);
        };

        $scope.dietGoals = [
            {
                label: "water",
                color: "#00b2ee",
                total: 5,
                goal: 10,
                percentage: 50,
                unit: "glass",
                units: "glasses",
                history: false,
                addedOn: ["07:30:00 GMT-0800 (Pacific Standard Time)","11:23:55 GMT-0800 (Pacific Standard Time)","15:40:39 GMT-0800 (Pacific Standard Time)", "17:32:09 GMT-0800 (Pacific Standard Time)", "19:12:33 GMT-0800 (Pacific Standard Time)"],
                dataPoints: []
            },
            {
                label: "coffee",
                color: "#8b5742",
                total: 2,
                goal: 3,
                percentage: 67,
                unit: "cup",
                units: "cups",
                history: false,
                addedOn: ["08:12:51 GMT-0800 (Pacific Standard Time)","13:42:05 GMT-0800 (Pacific Standard Time)"],
                dataPoints: [1,4,7,9]
            },
            {
                label: "vegetables",
                color: "#00cd00",
                total: 2,
                goal: 7,
                percentage: 29,
                unit: "serving",
                units: "servings",
                history: false,
                addedOn: ["08:23:39 GMT-0800 (Pacific Standard Time)", "12:32:09 GMT-0800 (Pacific Standard Time)"],
                dataPoints: [2,6,9,15]
            },
            {
                label: "fruit",
                color: "#ffa500",
                total: 1,
                goal: 4,
                percentage: 25,
                unit: "piece",
                units: "pieces",
                history: false,
                addedOn: ["08:28:35 GMT-0800 (Pacific Standard Time)"],
                dataPoints: [1,2]
            },
            {
                label: "carbohydrates",
                color: "#deb887",
                total: 1,
                goal: 3,
                percentage: 33,
                unit: "serving",
                units: "servings",
                history: false,
                addedOn: ["08:17:15 GMT-0800 (Pacific Standard Time)"],
                dataPoints: [3,5,8,21]
            },
            {
                label: "protein",
                color: "#b22222",
                total: 2,
                goal: 6,
                percentage: 33,
                unit: "serving",
                units: "servings",
                history: false,
                addedOn: ["08:20:09 GMT-0800 (Pacific Standard Time)", "08:20:10 GMT-0800 (Pacific Standard Time)"],
                dataPoints: [1,3]
            },
            {
                label: "junk food",
                color: "#000000",
                total: 1,
                goal: 2,
                percentage: 50,
                unit: "serving",
                units: "servings",
                history: false,
                addedOn: ["15:12:33 GMT-0800 (Pacific Standard Time)"],
                dataPoints: [7]
            }
        ];
        $scope.add = function (target){
            var timestamp = $scope.getTime();
            target.total++;
            target.percentage = Math.round(target.total / target.goal *100);
            target.addedOn.push(timestamp);
            target.dataPoints.push([timestamp, target.percentage, target.total , $scope.getSeconds(timestamp)]);
        };


/*
        "08:30:00 GMT","11:23:55 GMT","15:40:39 GMT", "17:32:09 GMT", "19:12:33 GMT", "22:45:11 GMT"
*/
       /* $scope.water = {
            total: 5,
            goal: 10,
            unit: "glasses",
            history: false,
            addedOn: ["08:30:00 GMT","11:23:55 GMT","15:40:39 GMT", "17:32:09 GMT", "19:12:33 GMT"]
        };
        $scope.addWater = function (){
            var time = $scope.getTime();
            $scope.water.total++;
            $scope.water.addedOn.push(time);
            $scope.datapoints.push([{
                type: 'Water',
                label: time,
                xValue: $scope.water.total,
                yValue: $scope.getSeconds(time)
            }]);
        };
        $scope.coffee = {
            total: 2,
            goal: 3,
            unit: "cups",
            history: false,
            addedOn: ["08:30:00 GMT","11:23:55 GMT"]
        };
        $scope.addCoffee = function (){
            $scope.coffee.total++;
            $scope.coffee.addedOn.push($scope.getTime());
        };
        $scope.vegetable = {
            total: 2,
            goal: 7,
            unit: "servings",
            history: false,
            addedOn: ["15:40:39 GMT", "17:32:09 GMT"]
        };
        $scope.addVegetable = function (){
            $scope.vegetable.total++;
            $scope.vegetable.addedOn.push($scope.getTime());
        };
        $scope.fruit = {
            total: 1,
            goal: 4,
            unit: "servings",
            history: false,
            addedOn: ["11:23:55 GMT"]
        };
        $scope.addFruit = function (){
            $scope.fruit.total++;
            $scope.fruit.addedOn.push($scope.getTime());
        };
        $scope.carbohydrates = {
            total: 1,
            goal: 3,
            unit: "servings",
            history: false,
            addedOn: ["11:23:55 GMT"]
        };
        $scope.addCarbohydrates = function (){
            $scope.carbohydrates.total++;
            $scope.carbohydrates.addedOn.push($scope.getTime());

        };
        $scope.protein = {
            total: 2,
            goal: 5,
            unit: "servings",
            history: false,
            addedOn: ["17:32:09 GMT", "19:12:33 GMT"]
        };
        $scope.addProtein = function (){
            $scope.protein.total++;
            $scope.protein.addedOn.push($scope.getTime());

        };
        $scope.junk = {
            total: 1,
            goal: 3,
            unit: "servings",
            history: false,
            addedOn: ["19:12:33 GMT"]
        };
        $scope.addJunk = function (){
            $scope.junk.total++;
            $scope.junk.addedOn.push($scope.getTime());

        };*/
    });