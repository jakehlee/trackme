A lightweight web app in angular that allows you to keep track of your daily intake of food and daily activity.

An animated line graph feature and form for making your own progress bars is in development as well.